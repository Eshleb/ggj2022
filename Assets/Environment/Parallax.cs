using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float Strength;
    private float length, startPos;
    
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 cameraPosition = MainCamera.Instance.transform.position;
        float temp = cameraPosition.x * (1 - Strength);
        float distance = cameraPosition.x * Strength;

        transform.position = new Vector3(startPos + distance, cameraPosition.y, transform.position.z);
        while (startPos + length < temp)
            startPos += length;
        while (temp < startPos - length)
            startPos -= length;
    }
}
