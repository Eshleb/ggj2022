using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    private void Start()
    {
        LevelManager.Instance.OnReset += Reenable;
    }

    private void Reenable()
    {
        gameObject.SetActive(true);
    }

    public void Collect()
    {
        Stability.Instance.ResetStability();
        AudioManager.Instance.PlaySound(Sound.Strawberry_collect);
        gameObject.SetActive(false);
    }

}
