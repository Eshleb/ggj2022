using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    public Vector2 Force;
    public Transform Left, Right;

    public SpriteRenderer Regular, Trashcan;

    public GameObject PaperPrefab, CanPrefb;
    public Transform ShootPoint;
    public Vector2 ShootSpeed;
    private bool shootThisBeat, wasAboveHalfBeatLastFrame;
    public Animator TrashcanAnimator;

    private void Start()
    {
        if (Vector2.zero == Force)
            Force = new Vector2(0, 800);

        Left = transform.GetChild(0);
        Right = transform.GetChild(1);

        Stability.Instance.OnSetNightmareMode += Transformation;
        Transformation(false);
    }

    private void Transformation(bool Nightmare)
    {
        Regular.enabled = !Nightmare;
        Trashcan.enabled = Nightmare;
    }

    private void Update()
    {
        if (Stability.Instance.IsNightmareMode)
        {
            bool belowHalfBeat = RhythmTracker.Instance.CalculateBeatFrac() < 0.5f;
            if (belowHalfBeat && wasAboveHalfBeatLastFrame)
            {
                if (shootThisBeat)
                {
                    TrashcanAnimator.SetTrigger("Shoot");
                    StartCoroutine(Shoot());
                }
                shootThisBeat = !shootThisBeat;
            }
            wasAboveHalfBeatLastFrame = !belowHalfBeat;
        }
    }
   
    private IEnumerator Shoot()
    {
        yield return new WaitForSeconds(0.7f);
        GameObject copy = Instantiate(Random.value < 0.5f ? PaperPrefab : CanPrefb);
        copy.transform.position = ShootPoint.position;
        copy.GetComponent<Rigidbody2D>().velocity = ShootSpeed;
        copy.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-1000f, 1000f);
        Destroy(copy, 10);
    }
}
