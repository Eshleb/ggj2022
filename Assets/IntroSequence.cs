using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroSequence : MonoBehaviour
{
    public AudioSource DreamSound;
    public Vector3 FinalPosition;
    public float FinalSize;
    void Start()
    {
        StartCoroutine(Sequence());
    }

    private IEnumerator Sequence()
    {
        yield return new WaitForSeconds(4);
        DreamSound.Play();
        yield return new WaitForSeconds(2);
        float startTime = Time.time, endTime = startTime + 3;
        Camera main = Camera.main;
        float startSize = main.orthographicSize;
        Vector3 startPosition = main.transform.position;
        while (Time.time < endTime)
        {
            float progress = Mathf.InverseLerp(startTime, endTime, Time.time);
            main.orthographicSize = Mathf.Lerp(startSize, FinalSize, progress);
            main.transform.position = Vector3.Lerp(startPosition, FinalPosition, progress);
            yield return null;
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level 1");
    }
}
