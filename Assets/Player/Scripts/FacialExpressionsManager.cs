using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacialExpressionsManager : MonoBehaviour
{
    public SpriteRenderer Shocked, Crying;
    public List<SpriteRenderer> Happy;

    private void Start()
    {
        if (Stability.Instance)
            Stability.Instance.Subscribe(OnStabilityChange);
    }

    private void OnStabilityChange(float Level, float _)
    {
        int index = Mathf.FloorToInt(3 * Mathf.InverseLerp(Stability.Instance.MinStability, Stability.Instance.MaxStability, Level));
        Debug.Log(Level);
        foreach (SpriteRenderer sr in Happy)
            sr.color = new Color(1, 1, 1, 1 < index ? 1 : 0);
        Shocked.color = new Color(1, 1, 1, 1 == index ? 1 : 0);
        Crying.color = new Color(1, 1, 1, 0 == index ? 1 : 0);
    }
}
