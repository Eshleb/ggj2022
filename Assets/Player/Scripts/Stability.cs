using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stability : SingletonMB<Stability>
{
    public float Level { get; private set; }

    public float MinStability, MaxStability;
    public float NightmareEnter, NightmareExit, NightmareRegen;

    public delegate void StabilityChangeEvent(float Level, float Effect);

    private event StabilityChangeEvent OnStabilityChanged;

    public event System.Action<bool> OnSetNightmareMode;
    public bool IsNightmareMode { get; private set; }

    private void Start()
    {
        LevelManager.Instance.OnReset += ResetStability;
        ResetStability();
    }

    private void Update()
    {
        if (IsNightmareMode)
            AffectStability(NightmareRegen * Time.deltaTime);
    }

    public void AffectStability(float Effect)
    {
        Level = Mathf.Clamp(Level + Effect, MinStability, MaxStability);
        OnStabilityChanged?.Invoke(Level, Effect);
        Debug.Log(Level);
        if (IsNightmareMode)
        {
            if (NightmareExit < Level)
            {
                IsNightmareMode = false;
                OnSetNightmareMode?.Invoke(false);
            }
        } 
        else
        {
            if (Level < NightmareEnter)
            {
                IsNightmareMode = true;
                OnSetNightmareMode?.Invoke(true);
            }
        }
    }

    public void ResetStability()
    {
        Level = MaxStability;
        OnStabilityChanged?.Invoke(Level, 0);
    }

    public void Subscribe(StabilityChangeEvent Callback, bool Invoke = false)
    {
        OnStabilityChanged += Callback;
        if (Invoke)
            Callback(Level, 0);
    }

    public void Unsubscribe(StabilityChangeEvent Callback, bool Invoke = false)
    {
        OnStabilityChanged -= Callback;
        if (Invoke)
            Callback(Level, 0);
    }
}
