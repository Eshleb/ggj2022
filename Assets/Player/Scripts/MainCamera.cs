using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class MainCamera : SingletonMB<MainCamera>
{
    public Vector3 PlayerOffset;

    [Range(2, 30)]
    public float FollowSpeed;

    public float DogSpeed;

    public float FadeTime;
    public List<SpriteRenderer> DogRenderers;
    public Collider2D DogCollider;

    private Vector3 _velocity;
    private Transform player;

    private bool chasingPlayer;
    private float targetPositionX;

    private void Start()
    {
        player = PlayerController.Instance.transform;
        transform.position = player.position + PlayerOffset;
        Stability.Instance.OnSetNightmareMode += SetNightmareMode;
    }

    private void SetNightmareMode(bool Enable)
    {
        StopAllCoroutines();
        StartCoroutine(FadeGrayscaleCRTN(Enable));
    }

    private IEnumerator FadeGrayscaleCRTN(bool Nightmare)
    {
        if (Nightmare)
        {
            foreach (SpriteRenderer sr in DogRenderers)
                sr.color = new Color(1, 1, 1, 1);
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else 
        {
            DogCollider.enabled = false;
            chasingPlayer = false;
        }
        PostProcessVolume volume = GetComponent<PostProcessVolume>();
        float startTime = Time.time, endTime = Time.time + FadeTime;
        while (Time.time < endTime)
        {
            float progress = Mathf.InverseLerp(startTime, endTime, Time.time);
            Fade(Nightmare ? progress : 1 - progress, !Nightmare);
            yield return null;
        }

        Fade(Nightmare ? 1 : 0, !Nightmare);
        targetPositionX = transform.position.x;
        if (Nightmare)
        {
            DogCollider.enabled = true;
            chasingPlayer = true;
        }
        else 
            transform.GetChild(0).gameObject.SetActive(false);

        void Fade(float nightmareFactor, bool fadeDog)
        {
            volume.profile.GetSetting<ColorGrading>().saturation.Override(Mathf.Lerp(0, -100, nightmareFactor));
            if (fadeDog)
                foreach (SpriteRenderer sr in DogRenderers)
                    sr.color = new Color(1, 1, 1, nightmareFactor);
        }
    }

    private void LateUpdate()
    {
        Vector3 targetPosition;
        if (chasingPlayer)
        {
            targetPositionX -= DogSpeed * Time.deltaTime;
            targetPosition = new Vector3(targetPositionX, player.position.y, player.position.z) + PlayerOffset;
        }
        else
        {
            targetPosition = player.position + PlayerOffset;
        }
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref _velocity, 1 / FollowSpeed);    
    }
}
