//#define TEST

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmTracker : SingletonMB<RhythmTracker>
{
    public float Offset;
    [Range(-0.2f, 0.2f)] public float ManualOffset;
    [Range(0.1f, 0.25f)]
    public float Fuzz = 0.1f;
    public float HealAmount, DamageMultiplier;

    private float _lastManualOffset;

    public void LevelStarted()
    {
        Offset = Time.time + ManualOffset;
    }

    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            Offset = Offset + ManualOffset - _lastManualOffset;
            _lastManualOffset = ManualOffset;
        }
    }

    public float CalculateBeatFrac()
    {
        float beatrate = 60 / GameManager.Instance.BPM;
        float time = Time.time - Offset;
        return (time % beatrate) / beatrate;
    }

    public float CalculateInaccuracy()
    {
        return Mathf.Max(0, Mathf.PingPong(CalculateBeatFrac(), 0.5f) - Fuzz) / (0.5f - Fuzz);
    }

    public float CalculateDamage()
    {
        float inaccuracy = CalculateInaccuracy();
        if (inaccuracy < Mathf.Epsilon)
        {
            return HealAmount;
        }
        else return -DamageMultiplier * inaccuracy;
    }

#if TEST
    public UnityEngine.UI.Text Test;

    private void Update()
    {
        if (0 < Offset) 
        Test.text = CalculateDamage().ToString();
    }
#endif
}
