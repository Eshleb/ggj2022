using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : SingletonMB<PlayerController>
{
    public KeyCode LeftKey = KeyCode.A, RightKey = KeyCode.D, JumpKey = KeyCode.Space, DodgeKey = KeyCode.Mouse0;

    public float Speed;
    public float AirControlForce;
    public float JumpForce;

    public float UpGravity, DownGravity;

    private Rigidbody2D rb;
    private Animator animator;

    [SerializeField]
    private bool canJump, isDodging;

    private float lastJumpTime = -1000;
    private Vector3 startPosition;

    private bool footstepSound2;
    public float StepSoundInterval;
    private float lastStepSoundTime = -1000;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        startPosition = transform.position;
        LevelManager.Instance.OnReset += OnResetLevel;
    }

    // Update is called once per frame
    void Update()
    {
        bool pressedDodge = Input.GetKeyDown(DodgeKey);
        if (Input.GetKeyDown(LeftKey) || Input.GetKeyDown(RightKey) || Input.GetKeyDown(JumpKey) || pressedDodge)
        {
            GameManager.Instance.PlayerInputReceived();
        }
        if (Input.GetKeyDown(KeyCode.Space) && Mathf.Abs(rb.velocity.y) < 0.01f)
        {
            Jump(Vector2.up * JumpForce);
            AudioManager.Instance.PlaySound(Sound.jump_1 + Random.Range(0, 3));
        }
        bool left = Input.GetKey(KeyCode.A), right = Input.GetKey(KeyCode.D);
        if (canJump)
            rb.velocity = new Vector2(left ? -Speed : right ? Speed : 0, rb.velocity.y);
        else
        {
            rb.AddForce(new Vector2(left ? -AirControlForce : right ? AirControlForce : 0, 0));
            rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -Speed, Speed), rb.velocity.y);
        }
        bool walking = left != right;
        HandleWalkingSound(walking);
        SetAnimatorParams(walking);

        if (pressedDodge && !isDodging)
            Dodge();

        if (transform.position.y < -10)
            LevelManager.Instance.RestartLevel();
        transform.localScale = new Vector3(rb.velocity.x < 0 ? -transform.localScale.y : 0 < transform.localScale.y ? transform.localScale.y : transform.localScale.x, transform.localScale.y, transform.localScale.z); ;
    }

    private void SetAnimatorParams(bool Walking)
    {
        animator.SetBool("Grounded", canJump);
        animator.SetBool("Walking", Walking);
    }

    private void Dodge()
    {
        isDodging = true;
        StartCoroutine(DodgeCRTN());
        animator.SetTrigger("Dodge");
    }

    private IEnumerator DodgeCRTN()
    {
        float startTime = Time.time, endTime = startTime + 60 / GameManager.Instance.BPM;
        while (true)
        {
            yield return null;
            float progress = Mathf.InverseLerp(startTime, endTime, Time.time);
            transform.rotation = Quaternion.AngleAxis(progress * 360, Vector3.up);
            if (0.999 < progress)
                break;
        }
        transform.rotation = Quaternion.identity;
        isDodging = false;
    }

    private void Jump(Vector2 Force)
    {
        canJump = false;
        lastJumpTime = Time.time;
        rb.AddForce(Force);
        animator.SetTrigger("Jump");
    }

    private void HandleWalkingSound(bool walking)
    {
        if (!walking)
            return;
        AudioManager am = AudioManager.Instance;
        if (!footstepSound2)
        {
            if (Time.time < lastStepSoundTime + StepSoundInterval)
                return;

            lastStepSoundTime = Time.time;
            footstepSound2 = true;
            am.PlaySound(Sound.footstep_leaves_2);
        }
        else
        {
            if (Time.time < lastStepSoundTime + StepSoundInterval)
                return;

            lastStepSoundTime = Time.time;
            footstepSound2 = false;
            am.PlaySound(Sound.footstep_leaves);
        }
    }

    private void FixedUpdate()
    {
        if (rb.velocity.y < 0)
            rb.gravityScale = DownGravity;
        else rb.gravityScale = UpGravity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Mushroom other = collision.collider.GetComponent<Mushroom>();
        if (null != other)
        {
            if (collision.collider.CompareTag("Finish")) 
            {
                LevelManager.Instance.GoToWakeup(true);
                collision.gameObject.GetComponent<AudioSource>().Play();
            }
            if (Stability.Instance.IsNightmareMode)
            {
                
            }
            else
            {
                AudioManager.Instance.PlaySound(Sound.mushroom_bounce);
                Jump(other.Force);
                other.GetComponent<Animator>().SetTrigger("Bounce");
                if (transform.position.y < other.Right.position.y)
                {
                    if (transform.position.x < other.transform.position.x)
                        transform.position = other.Left.position;
                    else transform.position = other.Right.position;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Collectible col = collision.GetComponent<Collectible>();
        if (null != col)
        {
            col.Collect();
            startPosition = transform.position;
        }
        else
        {
            MainCamera mc = collision.GetComponent<MainCamera>();
            if (null != mc)
                LevelManager.Instance.GoToWakeup(false);
            else
            {
                if (collision.CompareTag("Trash"))
                {
                    Stability.Instance.AffectStability(-15);
                }
                else
                {

                }
            }
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (canJump)
            return;
        if (Time.time < lastJumpTime + 0.2f)
            return;
        if (collision.collider.GetComponent<Mushroom>())
            return;
        foreach (ContactPoint2D point in collision.contacts)
        {
            if (0 < point.normal.y)
            {
                canJump = true;
                break;
            }
        }
    }


    private void OnResetLevel()
    {
        transform.position = startPosition;
        rb.velocity = Vector3.zero;
    }



    [Header("BPM")]
    public float BPM;
    public float Initial, Step;
    [ContextMenu("Find BPM")]
    public void FindBPM()
    {
        if (!Application.isPlaying)
        {
            Debug.LogWarning("Only call Find BPM in play mode.");
            return;
        }
        StartCoroutine(FindBPMCRTN());
    }

    private IEnumerator FindBPMCRTN()
    {
        float targetTime = 60 / BPM;
        float originalJF = JumpForce;
        JumpForce = Initial;
        float lastElapsed = 0;
        while (true)
        {
            float startTime = Time.time;
            JumpForce += Step;
            Jump(Vector2.up * JumpForce);
            yield return new WaitUntil(() => { return canJump; });
            float elapsed = Time.time - startTime;
            if (targetTime < elapsed)
            {
                Debug.Log($"Just below: {JumpForce - Step} at {60 / lastElapsed} BPM, just over: {JumpForce} at {60 / elapsed} BPM");
                break;
            }
            lastElapsed = elapsed;
        }
    }
}
