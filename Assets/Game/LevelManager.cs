using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 

public class LevelManager : SingletonMB<LevelManager>
{
    public event System.Action OnReset;
    public Image Overlay;

    public void LoadLevel(int Level)
    {
        SceneManager.LoadScene(Level);
        RestartLevel();
    }

    public void RestartLevel()
    {
        OnReset?.Invoke();
    }

    public void StartLevel()
    {
        RhythmTracker.Instance.LevelStarted();
    }

    public void GoToWakeup(bool GoodEnding)
    {
        if (!GoodEnding)
            new GameObject("BadEndingMarker").AddComponent<BadEndingMarker>();
        StartCoroutine(FadeSequence(GoodEnding));
    }

    private IEnumerator FadeSequence(bool Good)
    {
        float startTime = Time.time, endTime = startTime + 1;
        Color color = Good ? Color.white : Color.white;
        color.a = 0;
        while (Time.time < endTime)
        {
            float progress = Mathf.InverseLerp(startTime, endTime, Time.time);
            color.a = progress;
            Overlay.color = color;
            yield return null;
        }
        SceneManager.LoadScene("Wake");
    }
}
