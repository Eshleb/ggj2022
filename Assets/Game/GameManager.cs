using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonMB<GameManager>
{
    public float BPM;
    public bool FirstMove;
    public GameObject FieldTiles, CityTiles;
    public SpriteRenderer Gate;
    

    private void Start()
    {
        FirstMove = true;
        Stability.Instance.OnSetNightmareMode += SwapTiles;
    }

    private void SwapTiles(bool Nightmare)
    {
        FieldTiles.SetActive(!Nightmare);
        CityTiles.SetActive(Nightmare);
    }

    public void PlayerInputReceived()
    {
        if (FirstMove)
        {
            RhythmTracker.Instance.LevelStarted();
            FirstMove = false;
            AudioManager.Instance.PlaySound(Sound.dream_music);
            AudioManager.Instance.PlaySound(Sound.Nightmare_music);
            AudioManager.Instance.Sounds[(int)Sound.Nightmare_music].volume = 0;
        }
        else
        {
            float stabilityChange = RhythmTracker.Instance.CalculateDamage();
            Stability.Instance.AffectStability(stabilityChange);
        }
    }

    private void Update()
    {
        Gate.color = new Color(1,1,1, Mathf.MoveTowards(Gate.color.a, 0, Time.deltaTime));
    }
}
