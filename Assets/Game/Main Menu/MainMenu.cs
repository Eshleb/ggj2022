using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [Header("Transition")]
    public float SwapTime;
    public Transform[] Menus;

    [Header("Level Select")]
    public GameObject LevelSelectPrefab;
    public Transform SceneGrid;


    private void Start()
    {
        //AddLevels();
    }

    public void SwapMainToLevels()
    {
        SceneManager.LoadScene("Sleep");
    }

    public void SwapLevelsToMain()
    {
        SwapScreen(1, 0);
    }

    public void SwapMainToInfo()
    {
        SwapScreen(0, 2);
    }

    public void SwapInfoToMain()
    {
        SwapScreen(2, 0);
    }

    private void SwapScreen(int From, int To)
    {
        StopAllCoroutines();
        StartCoroutine(SwapScreenCRTN(From, To));
    }

    private IEnumerator SwapScreenCRTN(int From, int To)
    {
        float startTime = Time.time;
        while (true)
        {
            yield return null;
            float percent = (Time.time - startTime) / SwapTime;
            if (1 < percent)
                break;
            Menus[From].rotation = Quaternion.AngleAxis(90 * percent, Vector3.right);
            Menus[To].rotation = Quaternion.AngleAxis(90 * (1 - percent), Vector3.right);
        }
        Menus[From].rotation = Quaternion.AngleAxis(90, Vector3.right);
        Menus[To].rotation = Quaternion.AngleAxis(0, Vector3.right);
    }

    public void CloseGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    private void AddLevels()
    {
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            Scene s = SceneManager.GetSceneByBuildIndex(i);
            if (s.name.ToLower().StartsWith("level"))
            {
                AddScene(s);
            }
        }
    }

    private void AddScene(Scene Scene)
    {
        Instantiate(LevelSelectPrefab, SceneGrid).GetComponent<LevelSelector>().Initialize(Scene);
    }
}
