using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour
{
    public Text Nametag;
    private int buildIndex;

    public void Initialize(Scene Scene)
    {
        name = Scene.name;
        buildIndex = Scene.buildIndex;
        Nametag.text = Scene.name.Substring(6);
    }

    public void OnClick()
    {
        LevelManager.Instance.LoadLevel(buildIndex);
    }
}
