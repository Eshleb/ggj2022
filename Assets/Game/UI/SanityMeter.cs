using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanityMeter : MonoBehaviour
{
    public Image Fill;
    public Transform Cloud, StartPosition, EndPosition;
    public List<Transform> BeatLines;
    public float BeatLineSpeed;
    public Transform BeatLinesCenter;
    public float BeatLinesOffset;

    public float Speed;

    private float target;

    bool lastFrameWasAboveHalfBeat;

    private void Start()    
    {
        Stability.Instance.Subscribe(OnStabilityChanged);
        target = 1;
    }

    private void OnStabilityChanged(float Level, float Effect)
    {
        float levelPercent = Mathf.InverseLerp(Stability.Instance.MinStability, Stability.Instance.MaxStability, Level);
        target = levelPercent;
    }

    private void Update()
    {
        if (0 == RhythmTracker.Instance.Offset)
        {
            foreach (Transform line in BeatLines)
                line.gameObject.SetActive(false);
            return;
        }
        else
        {
            if (!BeatLines[0].gameObject.activeSelf)
            {
                foreach (Transform line in BeatLines)
                    line.gameObject.SetActive(true);
            }
        }
        Fill.fillAmount = Mathf.MoveTowards(Fill.fillAmount, target, Speed);
        Cloud.position = Vector3.Lerp(StartPosition.position, EndPosition.position, Fill.fillAmount);
        float beatFrac = RhythmTracker.Instance.CalculateBeatFrac();
        bool belowHalfBeat = 0.5f < beatFrac;
        if (belowHalfBeat && lastFrameWasAboveHalfBeat)
        {
            Transform temp = BeatLines[0];
            BeatLines.RemoveAt(0);
            BeatLines.Add(temp);
            temp = BeatLines[0];
            BeatLines.RemoveAt(0);
            BeatLines.Add(temp);
        }
        lastFrameWasAboveHalfBeat = !belowHalfBeat;
        for (int i = 0; i < 4; i++)
        {
            float distance = (1 + i - beatFrac + BeatLinesOffset) * BeatLineSpeed;
            BeatLines[i * 2].position = BeatLinesCenter.position + Vector3.left * distance;
            BeatLines[i * 2 + 1].position = BeatLinesCenter.position + Vector3.right * distance;
        }
    }
}
