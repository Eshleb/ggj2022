using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DamageOverlay : MonoBehaviour
{
    public Image Overlay;
    public AnimationCurve AlphaByFactor;

    private void Start()
    {
        Stability.Instance.Subscribe(OnDamage);
    }

    private void OnDestroy()
    {
        Stability.Instance.Unsubscribe(OnDamage);
    }

    private void OnDamage(float Stability, float Damage)
    {
        if (Damage < 0)
        {
            StartCoroutine(OnDamageCRTN(Damage / RhythmTracker.Instance.DamageMultiplier));
        }
    }

    private IEnumerator OnDamageCRTN(float Factor)
    {
        Overlay.color = new Color(Overlay.color.r, 0, 0, AlphaByFactor.Evaluate(-Factor));
        float spb = 60 / GameManager.Instance.BPM;
        float startTime = Time.time;
        float duration = 1 - 2 * (RhythmTracker.Instance.CalculateBeatFrac() % 0.5f);
        if (duration < spb / 4)
            duration += spb / 2;
        while (true)
        {
            yield return null;
            float progress = 1 - (Time.time - startTime) / duration;
            Overlay.color = new Color(Overlay.color.r, 0, 0, AlphaByFactor.Evaluate(-Factor * progress));
            if (progress < Mathf.Epsilon)
                yield break;
        }
    }
}
