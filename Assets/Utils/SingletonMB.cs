using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMB<T> : MonoBehaviour where T : SingletonMB<T> 
{
    public static T Instance { get; private set; }

    protected virtual void Awake()
    {
        if (null != Instance)
            Debug.LogError($"Multiple instances of {GetType().Name} present.");
        Instance = (T)this;
    }
}
