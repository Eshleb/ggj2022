using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sound
{
    dream_music, Nightmare_music, footstep_leaves, footstep_leaves_2, jump_1, jump_2, jump_3,
    jump_landing_leaves, dream_ambiance, nightmare_ambiance,
    mushroom_bounce, Strawberry_collect
}

public class AudioManager : SingletonMB<AudioManager>
{

    public AudioSource[] Sounds;
    public AnimationCurve FadeVolume;
    public float FadeTime;

    // Start is called before the first frame update
    void Start()
    {
        Stability.Instance.OnSetNightmareMode += FadeMusic; 
    }


    public void PlaySound(Sound sound)
    {
        Sounds[(int)sound].Play();
    }

    public void StopSound(Sound sound)
    {
        Sounds[(int)sound].Stop();
    }

    public bool IsPlaying(Sound sound)
    {
        return Sounds[(int)sound].isPlaying;
    }

    private void FadeMusic(bool Nightmare)
    {
        StopAllCoroutines();
        StartCoroutine(FadeMusicCRTN(Nightmare));
    }
    
    private IEnumerator FadeMusicCRTN(bool Nightmare) 
    {
        float startTime = Time.time, endTime = Time.time + FadeTime;
        while (Time.time < endTime)
        {
            float progress = Mathf.InverseLerp(startTime, endTime, Time.time);
            Fade(Nightmare ? progress : 1 - progress);
            yield return null;
        }

        Fade(Nightmare ? 1 : 0);

        void Fade(float nightmareFactor)
        {
            Sounds[(int)Sound.dream_music].volume = FadeVolume.Evaluate(1 - nightmareFactor);
            Sounds[(int)Sound.dream_ambiance].volume = FadeVolume.Evaluate(1 - nightmareFactor);
            Sounds[(int)Sound.Nightmare_music].volume = FadeVolume.Evaluate(nightmareFactor);
            Sounds[(int)Sound.nightmare_ambiance].volume = FadeVolume.Evaluate(nightmareFactor);
        }
    }
}
