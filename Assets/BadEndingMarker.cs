using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadEndingMarker : MonoBehaviour
{
    private void Start()
    {
        DontDestroyOnLoad(this);
    }
}
