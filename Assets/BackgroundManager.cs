using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    public List<GameObject> Field, City;

    private void Start()
    {
        Stability.Instance.OnSetNightmareMode += ChangeBackground;
        foreach (GameObject obj in City)
            obj.SetActive(false);
    }

    private void ChangeBackground(bool Nightmare)
    {
        foreach (GameObject obj in Field)
            obj.SetActive(!Nightmare);
        foreach (GameObject obj in City)
            obj.SetActive(Nightmare);
    }
}
