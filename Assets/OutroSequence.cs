using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutroSequence : MonoBehaviour
{
    [System.Serializable]
    public class Ending
    {
        [TextArea]
        public string Text;
        public AudioClip Sound;
    }

    public List<Ending> GoodEnding, BadEnding;
    public SpriteRenderer CryingFace, SleepyFace;
    public SpriteRenderer[] HappyFace;

    public UnityEngine.UI.Text Text;

    private void Start()
    {
        StartCoroutine(Sequence());
    }

    private IEnumerator Sequence()
    {
        yield return new WaitForSeconds(1);
        bool badEnding = null != FindObjectOfType<BadEndingMarker>();
        SleepyFace.enabled = false;
        if (badEnding)
        {
            CryingFace.enabled = true;
            foreach (SpriteRenderer sr in HappyFace)
                sr.enabled = false;
        }
        else
        {
            CryingFace.enabled = false;
            foreach (SpriteRenderer sr in HappyFace)
                sr.enabled = true;
        }
        yield return new WaitForSeconds(1);
        Ending chosenEnding;
        if (badEnding)
        {
            chosenEnding = BadEnding[Random.Range(0, BadEnding.Count)];
        }
        else
            chosenEnding = GoodEnding[Random.Range(0, GoodEnding.Count)];
        GetComponent<AudioSource>().clip = chosenEnding.Sound;
        GetComponent<AudioSource>().Play();
        Text.text = chosenEnding.Text;
    }

    public void Replay()
    {
        BadEndingMarker bem = FindObjectOfType<BadEndingMarker>();
        if (null != bem)
        {
            Destroy(bem);
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level 1");
    }
}
